from gitlab_demo_project.hello import call_hello

def test_hello():
    assert call_hello() == "hey, its me!"